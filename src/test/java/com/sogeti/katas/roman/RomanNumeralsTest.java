package com.sogeti.katas.roman;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RomanNumeralsTest {

    private final RomanNumerals romanNumerals = new RomanNumerals();

    @Test
    void givenNumber1_shouldReturnRomanI(){
        assertEquals("I", romanNumerals.arabicToRoman(1));
    }

    @Test
    void givenNumber3_shouldReturnRomanIII(){
        assertEquals("III", romanNumerals.arabicToRoman(3));
    }

    @Test
    void givenNumber9_shouldReturnRomanIX(){
        assertEquals("IX", romanNumerals.arabicToRoman(9));
    }

    @Test
    void givenNumber1066_shouldReturnRomanMLXVI(){
        assertEquals("MLXVI", romanNumerals.arabicToRoman(1066));
    }

    @Test
    void givenNumber1989_shouldReturnRomanMCMLXXXIX(){
        assertEquals("MCMLXXXIX", romanNumerals.arabicToRoman(1989));
    }


    @Test
    void givenNumber4999_shouldReturnRomanMCMLXXXIX(){
        assertEquals("MMMCMXCIX", romanNumerals.arabicToRoman(3999));
    }

    @Test
    void givenNumberO_shouldReturnEmptyString(){
        assertEquals("Error: number is out of range 1 to 3999", romanNumerals.arabicToRoman(0));
    }

    @Test
    void givenNegativeNumber10_shouldReturnEmptyString(){
        assertEquals("Error: number is out of range 1 to 3999", romanNumerals.arabicToRoman(-10));
    }

    @Test
    void givenNegativeNumber100_shouldReturnEmptyString(){
        assertEquals("Error: number is out of range 1 to 3999", romanNumerals.arabicToRoman(-100));
    }

    @Test
    void givenRomanI_shouldReturnNumber1(){
        assertEquals(1, romanNumerals.romanToArabic("I"));
    }

    @Test
    void givenRomanIII_shouldReturnNumber3(){
        assertEquals(3, romanNumerals.romanToArabic("III"));
    }

    @Test
    void givenRomanIX_shouldReturnNumber9(){
        assertEquals(9, romanNumerals.romanToArabic("IX"));
    }

    @Test
    void givenRomanMLXVIshouldReturnNumber1066(){
        assertEquals(1066, romanNumerals.romanToArabic("MLXVI"));
    }

    @Test
    void givenRomanMCMLXXXIXshouldReturnNumber1989(){
        assertEquals(1989, romanNumerals.romanToArabic("MCMLXXXIX"));
    }

    @Test
    void givenRomanMcmXXIshouldReturnNumber1921(){
        assertEquals(1921, romanNumerals.romanToArabic("McmXXI"));
    }

    @Test
    void givenRomanXiiIshouldReturnNumber13(){
        assertEquals(13, romanNumerals.romanToArabic("XiiI"));
    }

    @Test
    void givenInvalidRomanXIIIIshouldReturn0(){
        assertEquals(14, romanNumerals.romanToArabic("XIV"));
        assertEquals(0, romanNumerals.romanToArabic("XIIII"));
    }

    @Test
    void givenInvalidRomanXIIIIIshouldReturn0(){
        assertEquals(15, romanNumerals.romanToArabic("XV"));
        assertEquals(0, romanNumerals.romanToArabic("XIIIII"));
    }

    @Test
    void givenInvalidRomanXXXXshouldReturn0(){
        assertEquals(40, romanNumerals.romanToArabic("XL"));
        assertEquals(0, romanNumerals.romanToArabic("XXXX"));
    }

    @Test
    void givenInvalidRomanXXXXXshouldReturn0(){
        assertEquals(50, romanNumerals.romanToArabic("L"));
        assertEquals(0, romanNumerals.romanToArabic("XXXXX"));
    }

    @Test
    void givenInvalidRomanCCCCshouldReturn0(){
        assertEquals(400, romanNumerals.romanToArabic("CD"));
        assertEquals(0, romanNumerals.romanToArabic("CCCC"));
    }

    @Test
    void givenInvalidRomanCCCCCshouldReturn0(){
        assertEquals(500, romanNumerals.romanToArabic("D"));
        assertEquals(0, romanNumerals.romanToArabic("CCCCC"));
    }

    @Test
    void givenInvalidRomanMMMMshouldReturn0(){
        assertEquals(0, romanNumerals.romanToArabic("MMMM"));
    }

    @Test
    void givenInvalidRomanDDshouldReturn0(){
        assertEquals(1000, romanNumerals.romanToArabic("M"));
        assertEquals(0, romanNumerals.romanToArabic("DD"));
    }

    @Test
    void givenInvalidRomanVVshouldReturn0(){
        assertEquals(10, romanNumerals.romanToArabic("X"));
        assertEquals(0, romanNumerals.romanToArabic("VV"));
    }

    @Test
    void givenInvalidRomanNumeralshouldReturnNumber0(){
        assertEquals(0, romanNumerals.romanToArabic("INVALID ROMAN NUMERAL"));
    }

    @Test
    void givenEmptyStringshouldReturnNumber0(){
        assertEquals(0, romanNumerals.romanToArabic(""));
    }

    @Test
    void givenNullshouldReturnNumber0(){
        assertEquals(0, romanNumerals.romanToArabic(null));
    }
}