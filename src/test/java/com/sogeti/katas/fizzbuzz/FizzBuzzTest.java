package com.sogeti.katas.fizzbuzz;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class FizzBuzzTest {

    private FizzBuzz fizzBuzz = new FizzBuzz().addSubstitution(3, "Fizz").addSubstitution(5, "Buzz");
    private FizzBuzz fizzBuzzPop = new FizzBuzz().addSubstitution(3, "Fizz").addSubstitution(5, "Buzz").addSubstitution(7, "Pop");
    private FizzBuzz customFizzBuzz = new FizzBuzz().addSubstitution(2, "Fuzz").addSubstitution(3, "Bizz");

    @Test
    void givenZero_shouldReturnFizzBuzz(){
        assertEquals("Fizz Buzz", fizzBuzz.fizzbuzz(0));
    }

    @Test
    void givenNormalNumber1_shouldReturn1(){
        assertEquals("1", fizzBuzz.fizzbuzz(1));
    }

    @Test
    void givenNormalNumber2_shouldReturn2(){
        assertEquals("2", fizzBuzz.fizzbuzz(2));
    }

    @Test
    void givenNormalNumber4_shouldReturn4(){
        assertEquals("4", fizzBuzz.fizzbuzz(4));
    }

    @Test
    void givenNormalNumberNegative4_shouldReturnNegative4(){
        assertEquals("-4", fizzBuzz.fizzbuzz(-4));
    }

    @Test
    void givenMultipleOfThree3_shouldReturnFizz(){
        assertEquals("Fizz", fizzBuzz.fizzbuzz(3));
    }

    @Test
    void givenMultipleOfThree9_shouldReturnFizz(){
        assertEquals("Fizz", fizzBuzz.fizzbuzz(9));
    }

    @Test
    void givenMultipleOfThree123_shouldReturnFizz(){
        assertEquals("Fizz", fizzBuzz.fizzbuzz(123));
    }

    @Test
    void givenMultipleOfThreeNegative9_shouldReturnFizz(){
        assertEquals("Fizz", fizzBuzz.fizzbuzz(-9));
    }

    @Test
    void givenMultipleOfFive5_shouldReturnBuzz(){
        assertEquals("Buzz", fizzBuzz.fizzbuzz(5));
    }

    @Test
    void givenMultipleOfFive20_shouldReturnBuzz(){
        assertEquals("Buzz", fizzBuzz.fizzbuzz(20));
    }

    @Test
    void givenMultipleOfFive200_shouldReturnBuzz(){
        assertEquals("Buzz", fizzBuzz.fizzbuzz(200));
    }


    @Test
    void givenMultipleOfFiveNegative10_shouldReturnBuzz(){
        assertEquals("Buzz", fizzBuzz.fizzbuzz(-10));
    }

    @Test
    void givenMultipleOfThreeAndFive15_shouldReturnFizzBuzz(){
        assertEquals("Fizz Buzz", fizzBuzz.fizzbuzz(15));
    }

    @Test
    void givenMultipleOfThreeAndFive45_shouldReturnFizzBuzz(){
        assertEquals("Fizz Buzz", fizzBuzz.fizzbuzz(45));
    }

    @Test
    void givenMultipleOfThreeAndFive315_shouldReturnFizzBuzz(){
        assertEquals("Fizz Buzz", fizzBuzz.fizzbuzz(315));
    }

    @Test
    void givenMultipleOfThreeAndFiveNegative30_shouldReturnFizzBuzz(){
        assertEquals("Fizz Buzz", fizzBuzz.fizzbuzz(-30));
    }

    @Test
    void givenMultipleOfSeven7_shouldReturnPop(){
        assertEquals("Pop", fizzBuzzPop.fizzbuzz(7));
    }

    @Test
    void givenMultipleOfSeven28_shouldReturnPop(){
        assertEquals("Pop", fizzBuzzPop.fizzbuzz(28));
    }

    @Test
    void givenMultipleOfSeven77_shouldReturnPop(){
        assertEquals("Pop", fizzBuzzPop.fizzbuzz(77));
    }

    @Test
    void givenMultipleOfSevenNegative14_shouldReturnPop(){
        assertEquals("Pop", fizzBuzzPop.fizzbuzz(-14));
    }

    @Test
    void givenMultipleOfThreeAndSeven21_shouldReturnFizzPop(){
        assertEquals("Fizz Pop", fizzBuzzPop.fizzbuzz(21));
    }

    @Test
    void givenMultipleOfThreeAndSeven63_shouldReturnFizzPop(){
        assertEquals("Fizz Pop", fizzBuzzPop.fizzbuzz(63));
    }

    @Test
    void givenMultipleOfThreeAndSeven126_shouldReturnFizzPop(){
        assertEquals("Fizz Pop", fizzBuzzPop.fizzbuzz(126));
    }

    @Test
    void givenMultipleOfThreeAndSevenNegative42_shouldReturnFizzPop(){
        assertEquals("Fizz Pop", fizzBuzzPop.fizzbuzz(-42));
    }

    @Test
    void givenMultipleOfFiveAndSeven35_shouldReturnBuzzPop(){
        assertEquals("Buzz Pop", fizzBuzzPop.fizzbuzz(35));
    }

    @Test
    void givenMultipleOfThreeAndSeven70_shouldReturnBuzzPop(){
        assertEquals("Buzz Pop", fizzBuzzPop.fizzbuzz(70));
    }

    @Test
    void givenMultipleOfThreeAndSeven140_shouldReturnBuzzPop(){
        assertEquals("Buzz Pop", fizzBuzzPop.fizzbuzz(140));
    }

    @Test
    void givenMultipleOfThreeAndSevenNegative70_shouldReturnBuzzPop(){
        assertEquals("Buzz Pop", fizzBuzzPop.fizzbuzz(-70));
    }

    @Test
    void givenMultipleOfThreeAndFiveAndSeven105_shouldReturnFizzBuzzPop(){
        assertEquals("Fizz Buzz Pop", fizzBuzzPop.fizzbuzz(105));
    }

    @Test
    void givenMultipleOfThreeAndFiveAndSeven210_shouldReturnFizzBuzzPop(){
        assertEquals("Fizz Buzz Pop", fizzBuzzPop.fizzbuzz(210));
    }

    @Test
    void givenMultipleOfThreeAndFiveAndSeven315_shouldReturnFizzBuzzPop(){
        assertEquals("Fizz Buzz Pop", fizzBuzzPop.fizzbuzz(315));
    }

    @Test
    void givenMultipleOfThreeAndFiveAndSevenNegative105_shouldReturnFizzBuzzPop(){
        assertEquals("Fizz Buzz Pop", fizzBuzzPop.fizzbuzz(-105));
    }

    @Test
    void givenCustomMultipleTwoAsFuzz1_shouldReturn1(){
        assertEquals("1", customFizzBuzz.fizzbuzz(1));
    }

    @Test
    void givenCustomMultipleTwoAsFuzz2_shouldReturnFuzz(){
        assertEquals("Fuzz", customFizzBuzz.fizzbuzz(2));
    }

    @Test
    void givenCustomMultipleTwoAsFuzz8_shouldReturnFuzz(){
        FizzBuzz customFizzBuzz = new FizzBuzz().addSubstitution(2, "Fuzz");
        assertEquals("Fuzz", customFizzBuzz.fizzbuzz(8));
    }

    @Test
    void givenCutomMultipleOfTwoAsFuzzAndThreeAsBizz4_shouldReturnFuzzBizz(){

        assertEquals("Fuzz", customFizzBuzz.fizzbuzz(4));
    }

    @Test
    void givenCutomMultipleOfTwoAsFuzzAndThreeAsBizz9_shouldReturnFuzzBizz(){
        assertEquals("Bizz", customFizzBuzz.fizzbuzz(9));
    }

    @Test
    void givenCutomMultipleOfTwoAsFuzzAndThreeAsBizz12_shouldReturnFuzzBizz(){
        assertEquals("Fuzz Bizz", customFizzBuzz.fizzbuzz(12));
    }

    @Test
    void givenMultipleOfThreeAndFiveAndSevenAndCustomTwoAsFuzz210_shouldReturnFuzzFizzBuzzPop(){
        FizzBuzz customFizzBuzz = new FizzBuzz().addSubstitution(2, "Fuzz").addSubstitution(3, "Fizz")
                .addSubstitution(5, "Buzz").addSubstitution(7, "Pop");
        assertEquals("Fuzz Fizz Buzz Pop", customFizzBuzz.fizzbuzz(210));
    }

    @Test
    void givenNoSubstitutionsWith3_shouldReturn3(){
        FizzBuzz noSubFizzBuzz = new FizzBuzz();
        assertEquals("3", noSubFizzBuzz.fizzbuzz(3));
    }

    @Test
    void givenNoSubstitutionsWith5_shouldRetur5(){
        FizzBuzz noSubFizzBuzz = new FizzBuzz();
        assertEquals("5", noSubFizzBuzz.fizzbuzz(5));
    }

    @Test
    void givenNoSubstitutionsWith7_shouldReturn7(){
        FizzBuzz noSubFizzBuzz = new FizzBuzz();
        assertEquals("7", noSubFizzBuzz.fizzbuzz(7));
    }
}