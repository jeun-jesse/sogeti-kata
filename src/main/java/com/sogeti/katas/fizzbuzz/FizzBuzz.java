package com.sogeti.katas.fizzbuzz;

import java.util.Map;
import java.util.TreeMap;

public class FizzBuzz {

    private Map<Integer, String> map = new TreeMap<>();
    
    public String fizzbuzz(int num){
        final StringBuilder result = new StringBuilder();
        this.map.forEach((key, value) -> {
            if(num % key == 0) result.append(value).append(" ");
        });
        return (result.toString().isEmpty()) ? String.valueOf(num) : result.toString().trim();
    }
    
    public FizzBuzz addSubstitution(int num, String sub){
        this.map.put(num, sub);
        return this;
    }
}
