package com.sogeti.katas.roman;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RomanNumerals {

    private enum ConversionTable {
        M(1000),
        CM(900),
        D(500),
        CD(400),
        C(100),
        XC(90),
        L(50),
        XL(40),
        X(10),
        IX(9),
        V(5),
        IV(4),
        I(1);

        private int num;

        ConversionTable(int num){
            this.num = num;
        }
    }

    public String arabicToRoman(int num){
        if(num > 3999 || num < 1) return "Error: number is out of range 1 to 3999";
        StringBuilder result = new StringBuilder();
        for(ConversionTable ct : ConversionTable.values()){
            while(num >= ct.num){
                result.append(ct.name());
                num -= ct.num;
            }
        }
        return result.toString();
    }

    public int romanToArabic(String roman){
        if(roman == null || roman.isEmpty() || isInvalidRoman(roman)) return 0;
        roman = roman.toUpperCase();
        int result = 0;
        for(ConversionTable ct : ConversionTable.values()){
            int numeralCount = 0;
            while(roman.startsWith(ct.name()) && numeralCount < 3){
                result += ct.num;
                roman = roman.substring(ct.name().length());
                if(isNotRepeatableRoman(ct.name())) break;
                numeralCount++;
            }
        }
        return (roman.length() > 0) ? 0 : result;
    }

    private boolean isInvalidRoman(String roman) {
        roman = roman.toUpperCase();
        List<String> romanNumerals = Stream.of(ConversionTable.values()).map(Enum::name).collect(Collectors.toList());
        for(char c : roman.toCharArray()){
            if(!romanNumerals.contains(String.valueOf(c))) return true;
        }
        return false;
    }

    private boolean isNotRepeatableRoman(String roman){
        List<String> repeatableRoman = Arrays.asList("I", "X", "C", "M");
        return !repeatableRoman.contains(roman);
    }
}
